package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    
    @Test
    public void testGetX(){
        Vector3d vector = new Vector3d(1, 2, 3);
        //tolerance: the more number, the mode specific. the less zeros, the less specific. 5 zeros is arbitary by Dan teacher.
        assertEquals("testing if getX() returns the actual X input", 1, vector.getX(), 0.000001);
    }

    @Test
    public void testGetY(){
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(2, vector.getY(), 0.000001);
    }

    
    @Test
    public void testGetZ(){
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(3, vector.getZ(), 0.000001);
    }

    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), vector.magnitude(), 0.000001);
    }

    @Test 
    public void testDotProduct(){
        Vector3d vector = new Vector3d(1, 2, 3);
        Vector3d secondVector = new Vector3d(4, 5, 6);
        assertEquals(32, vector.dotProduct(secondVector), 0.000001);
    }

    @Test
    public void testAdd(){
        Vector3d vector = new Vector3d(1, 2, 3);
        Vector3d secondVector = new Vector3d(4, 5, 6);
        Vector3d newVector = vector.add(secondVector);
        assertEquals(5, newVector.getX(), 0.000001);
        assertEquals(7, newVector.getY(), 0.000001);
        assertEquals(9, newVector.getZ(), 0.000001);
    }
}
